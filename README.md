# Changeling Master

Master repo for the Changeling VR game. Learn more about the game at [ChangelingVr.com](https://www.changelingvr.com). Changeling is an upcoming VR title born from a team with a love of ludonarrative gameplay and compelling storytelling. The game will be shipping end of Summer 2020.



## The Development Team:

### Production/Oversight:

- Elouise "Weez" Oyzon, Supervisor

- David Simkins, Narrative Consultant

- Cody Van de Mark, Technical Consultant



### Art Team:

- Calise Jin, Art Lead

- Vincent Bennet

- Wren Bernstein

- Joseph Hong

- Allie Zhao



### Development Team:

- Justin Vaughn, Tech Lead

- Lucas Veldman

- Michael Aspinall

- Owen King

- Alfie Lou

- Michael "Mike" Benton

- Karl Sun

- Devon Grant

- Dillon Chan



### Web Team:

- Justin Moniquette, Web Lead

- Matthew Palermo

- Ilana Leva

- Ike Aja

- Gregory Gonzalez

- Zack Dunham



### Developed with Unreal Engine 4
